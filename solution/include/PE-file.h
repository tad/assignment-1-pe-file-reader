//
// Created by Never on 2/27/2023.
//
/// @file

#ifndef LAB1_PE_FILE_H
#define LAB1_PE_FILE_H
#include "PE-header.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h> 

#ifdef _MSK_VER
#pragma pack(push, 1)
#endif

struct
#ifdef __GNUC__
__attribute__((packed))
#endif
 PEFile {
    struct DOSHeader dos_header;
    struct PEHeader header;
    struct SectionHeader *section_headers;
};

/// @brief read DOS header, that contain information of PE file
/// @param file : pointer to input file
/// @param obj : DOSHeader object, with constructed memory
/// @return false if cannot read data to object, otherwise return true
bool read_DOS_header(FILE* file, struct DOSHeader* obj);

///@brief read data from input file to file header object
/// @param file: pointer to input file
/// @param pe_header: file header object
/// @param offset: offset to file header section
/// @return false if cannot read data to object, otherwise return true
bool read_file_header(FILE* file,struct PEHeader *pe_header,
                                unsigned char offset);

/// @brief find pointer to description of needed section at section header part
/// @param file: pointer to input file
/// @param number_of_sections: number of sections at that file
/// @param offset: offset to section header
/// @param section_to_find: name of section we need to find
/// @return pointer to description of section header, NULL if section not found
struct SectionHeader* find_section(FILE* file,
                                    unsigned short offset,
                                    char* section_to_find,
                                    uint16_t number_of_sections);

/// @brief write data of needed section to a temporary variable
/// @param in: pointer of input file
/// @param offset: offset of needed section
/// @param size: size of need section
/// @return control to temporary variable
void *extract_object(FILE* in, uint16_t offset, uint16_t size);

/// @brief write temporary variable content to output file
/// @param description: variable that contain content of needed section
/// @param size: size of needed section
/// @param to_write: output file
/// @return true if write to output file successfully, otherwise return false
bool extract_to_file(void* description,
                     FILE * to_write,
                     uint16_t size);
#endif //LAB1_PE_FILE_H
