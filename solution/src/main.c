//
// Created by Never on 2/27/2023.
//
/// @file
/// @brief Main application file

#include "../include/utils.h"
#include "../include/constant.h"

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
    (void) argc; (void) argv;
    if(argc != NUM_OF_ARGS) {
        fprintf(stderr, NUM_OF_ARGS_ERROR);
        return SUCCESS;
    }
    FILE* in_file = fopen(argv[1], "rb");
//    check if file input is open successfully
    if(!in_file) {
        if(!fclose(in_file)) {
            fprintf(stderr, CLOSE_FILE_ERROR);
            return UNEXPECTED_ERROR;
        }
        fprintf(stderr, OPEN_FILE_ERR);
        return UNEXPECTED_ERROR;
    };
//    Read DOS Header
    struct DOSHeader *dos_header= malloc(sizeof(struct DOSHeader));
    if(!read_DOS_header(in_file, dos_header)) {
        if(!fclose(in_file)) {
            free(dos_header);
            fprintf(stderr, CLOSE_FILE_ERROR);
            return UNEXPECTED_ERROR;
        }
        fprintf(stderr,"1");
        return UNEXPECTED_ERROR;
    }

    struct PEHeader *pe_header = malloc(sizeof(struct PEHeader));
    if(!read_file_header(in_file, pe_header,
                         dos_header->AddressOfFileHeader)){
        if(pe_header) free(pe_header);
        
        if(!fclose(in_file)) {
            fprintf(stderr, CLOSE_FILE_ERROR);
            return UNEXPECTED_ERROR;
        }
        fprintf(stderr,"2");
        return UNEXPECTED_ERROR;
    }
    print_header(pe_header);

    if(!pe_header) return UNEXPECTED_ERROR;
    uint16_t pointer_to_section_header =  (dos_header->AddressOfFileHeader + 4 + // offset to section header
                                                    sizeof(struct PEHeader) +
                                                            pe_header->SizeOfOptionalHeader);
    char *name = argv[2]; // get name of section
    int i = 0;
    while(i < 8 && name[i] != 0) ++i;
    char name_to_find[8] = {name[0],
                            name[1],
                            name[2],
                            name[3],
                            name[4],
                            name[5],
                            name[6],
                            name[7]};
    for(int j = i; j < 8; ++j) name_to_find[j] = 0; // reconvert section name arguments to uint8_t[8],
                                                    // because it may contain trash value after first 0 value
                                                    // ex: 15 26 48 23 0 25 4 0 -> 15 26 48 23 0 0 0 0

    struct SectionHeader *section_header = NULL;
    section_header = find_section(in_file, // find pointer to description of needed section
                                            pointer_to_section_header,
                                            name_to_find,
                                            pe_header->NumberOfSections);
    if(!section_header) {
        fprintf(stderr, READ_FILE_ERROR);
        free(section_header);
        return UNEXPECTED_ERROR;
    }
    FILE* out_file = fopen(argv[3], "wb");
    void * object_extract = extract_object(in_file, // create an object that contain data of section extracted
                                           section_header->PointerToRawData ,
                                           section_header->SizeOfRawData);
    if(!object_extract) {
        free(object_extract);
        fprintf(stderr, WRITE_FILE_ERROR);
        return UNEXPECTED_ERROR;
    }
    if(!extract_to_file(object_extract,  // write data to output file
                        out_file,
                        section_header->SizeOfRawData)) {
        fprintf(stderr, EXTRACT_FILE_ERROR);
        return UNEXPECTED_ERROR;
    }
    free(dos_header);
    free(pe_header);
    free(section_header);
    free(object_extract);
    if(!fclose(in_file)) {
        fprintf(stderr, CLOSE_FILE_ERROR);
    }
    if(!fclose(out_file)){
        fprintf(stderr, CLOSE_FILE_ERROR);
    }
    return 0;
}
