//
// Created by Never on 2/27/2023.
//
#include "../include/PE-file.h"

bool read_DOS_header(FILE* file, struct DOSHeader* obj)
{
    if(!fread(obj, sizeof(struct DOSHeader), 1, file))
    {
        return false;
    }
    printf("%hu \n", obj->MagicNumber);
    return true;
}

bool read_file_header(FILE* file,struct PEHeader *pe_header, uint8_t offset) {
    fseek(file, offset + 0x04, SEEK_SET);
    if(fread(pe_header, sizeof(struct PEHeader), 1, file) != 1) return false;
    printf("%hu \n", pe_header->NumberOfSections);
    return true;
}

struct SectionHeader* find_section(FILE* file,
                                      uint16_t offset,
                                      char* section_to_find,
                                      uint16_t number_of_sections)
{
    struct SectionHeader* res = malloc(sizeof(struct SectionHeader));
    printf("%d \n", offset);
    fseek(file, offset, SEEK_SET);
    for(int i = 0 ;i < number_of_sections; ++i) {
        if(!fread(res, sizeof(struct SectionHeader), 1, file)) {
            free(res);
            return NULL;
        }
        printf("res:%s  sec:%s", res->Name, section_to_find);
        int isEqual = 1;
        for(int j = 0 ;j < 8; ++j) {
            if(res->Name[j] != section_to_find[j]) {
                isEqual = 0;
            }
        }
        printf("\n");
        if(isEqual == 1) return res;
    }
    free(res);
    return NULL;
}

void *extract_object(FILE* in,
                     uint16_t offset,
                     uint16_t size) {
    void* res = malloc(size* sizeof(uint8_t));
    fseek(in, offset, SEEK_SET);
    if(fread(res, size, 1, in) != 1) return NULL;
    return res;
}

bool extract_to_file(void* description, FILE * to_write, uint16_t size)
{
    if(!fwrite(description, size, 1, to_write)) return false;
    return true;
}

