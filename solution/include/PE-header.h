//
// Created by Never on 2/27/2023.
//
/// @file
#ifndef LAB1_PE_HEADER_H
#define LAB1_PE_HEADER_H
#define IMAGE_NUMBER_OF_DIRECTORY_ENTRIES 16
#define IMAGE_SIZE_OF_SHORT_NAME 8
#define NUMBER_OF_WORD 4
#define NUMBER_OF_REVERSE_WORDS 10
#include <stdint.h>
#include <unistd.h>

#ifdef _MSK_VER
#pragma pack(push, 1)
#endif

struct 
#ifdef __GNUC__
__attribute__((packed))
#endif
DOSHeader {
    /// Magic number
     uint16_t MagicNumber;

     /// Bytes on last page of file
     uint16_t BytesOnLastPage;

     /// Pages in file
     uint16_t PagesOnFile;
    
    /// Relocations
     uint16_t Relocations;
     
     /// Size of header in paragraphs
     uint16_t SizeOfHeader;

     /// Minimum extra paragraphs needed
     uint16_t MinimumExtraParagraphNeeded;

     /// Maximum extra paragraphs needed
     uint16_t MaximumExtraParagraphNeeded;

     /// Initial (relative) SS value
     uint16_t InitialSSValue;

     /// Initial SP value
     uint16_t InitialSPValue;

    /// Checksum
     uint16_t Checksum;

     /// Initial IP value
     uint16_t InitialIPValue;

     /// Initial (relative) CS value
     uint16_t InitialCSValue;

     /// File address of relocation table
     uint16_t FileAddressOfRelocationTable;

     /// Overlay number
     uint16_t OverlayNumber;

     /// Reserved words
     uint16_t ReservedWords[NUMBER_OF_WORD];

     /// OEM identifier (for e_oeminfo)
     uint16_t OEMIdentifier;

     /// OEM information; e_oemid specific
     uint16_t OEMInformation;

     /// Reserved words
     uint16_t ReserveWords[NUMBER_OF_REVERSE_WORDS];

    /// File address of new exe header
    uint8_t AddressOfFileHeader;

};
struct 
#ifdef __GNUC__
__attribute__((packed))
#endif
PEHeader {
    ///The number that identifies the type of target machine
     uint16_t  Machine;

    ///The number of sections. This indicates the size of the section table, which immediately follows the headers.
     uint16_t  NumberOfSections;

    ///The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created.
    uint32_t TimeDateStamp;

    ///The file offset of the COFF symbol table, or zero if no COFF symbol table is present.
    uint32_t PointerToSymbolTable;

    ///The number of entries in the symbol table.
    uint32_t NumberOfSymbols;

    ///The size of the optional header, which is required for executable files but not for object files
    uint16_t  SizeOfOptionalHeader;

    ///The flags that indicate the attributes of the file
    uint16_t  Characteristics;

};

struct 
#ifdef __GNUC__
__attribute__((packed))
#endif
DataDirectory {
    uint32_t VirtualAddress;
    uint32_t Size;
};


struct 
#ifdef __GNUC__
__attribute__((packed))
#endif
SectionHeader {
        /// name of section header
    char  Name[IMAGE_SIZE_OF_SHORT_NAME];

    union {
        uint32_t PhysicalAddress;
        uint32_t VirtualSize;
    } Misc;

    /// address of header in virtual memory
    uint32_t VirtualAddress;

    /// size of raw data
    uint32_t SizeOfRawData;

    /// pointer to raw data
    uint32_t PointerToRawData;

    /// pointer to relocations
    uint32_t PointerToRelocations;

    /// pointer to line number
    uint32_t PointerToLineNumbers;

    /// number of relocation
     uint16_t  NumberOfRelocations;

    /// number of lines
     uint16_t  NumberOfLineNumbers;

    /// characteristic
    uint32_t Characteristics;

};
#endif //LAB1_PE_HEADER_H
