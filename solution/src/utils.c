//
// Created by Never on 2/28/2023.
//

#include "../include/utils.h"

void print_header(struct PEHeader *to_print) {
    printf("\n File header: \n");
    printf("Machine: %"PRIu16"\n",to_print->Machine);
    printf("Number Of Section: %"PRIu16"\n", to_print->NumberOfSections);
    printf("Time Date Stamp: %"PRIu32"\n", to_print->TimeDateStamp);
    printf("Pointer to symbol table: %"PRIu32"\n", to_print->PointerToSymbolTable);
    printf("Number of symbol: %"PRIu32"\n", to_print->NumberOfSymbols);
    printf("Size of optional header: %"PRIu16"\n", to_print->SizeOfOptionalHeader);
    printf("Characteristics: %"PRIu16"\n", to_print->Characteristics);
}

void print_dos_header(struct DOSHeader *to_print) {
    printf("\n DOS header: \n");
    printf("MagicNumber: %"PRIu16"\n",to_print->MagicNumber);
    printf("BytesOnLastPage: %"PRIu16"\n",to_print->BytesOnLastPage);
    printf("PagesOnFile: %"PRIu16"\n",to_print->PagesOnFile);
    printf("Relocations: %"PRIu16"\n",to_print->Relocations);
    printf("SizeOfHeader: %"PRIu16"\n",to_print->SizeOfHeader);
    printf("MinimumExtraParagraphNeeded: %"PRIu16"\n",to_print->MinimumExtraParagraphNeeded);
    printf("MaximumExtraParagraphNeeded: %"PRIu16"\n",to_print->MaximumExtraParagraphNeeded);
    printf("InitialSSValue: %"PRIu16"\n",to_print->InitialSSValue);
    printf("InitialSPValue: %"PRIu16"\n",to_print->InitialSPValue);
    printf("Checksum: %"PRIu16"\n",to_print->Checksum);
    printf("InitialIPValue: %"PRIu16"\n",to_print->InitialIPValue);
    printf("InitialCSValue: %"PRIu16"\n",to_print->InitialCSValue);
    printf("FileAddressOfRelocationTable: %c\n",to_print->FileAddressOfRelocationTable);
    printf("OverlayNumber: %"PRIu16"\n",to_print->OverlayNumber);
    printf("ReservedWords: %"PRIu16"\n",to_print->ReservedWords[0]);
    printf("OEMIdentifier: %"PRIu16"\n",to_print->OEMIdentifier);
    printf("OEMInformation: %"PRIu16"\n",to_print->OEMInformation);
    printf("ReserveWords: %"PRIu16"\n",to_print->ReserveWords[0]);
}

void print_section_header(struct SectionHeader *section) {
    printf("Section header: \n");
    printf("Name: %s \n", section->Name);
    printf("VirtualAddress: %"PRIu32" \n", section->VirtualAddress);
    printf("SizeOfRawData: %"PRIu32" \n", section->SizeOfRawData);
    printf("PointerToRawData: %"PRIu32" \n", section->PointerToRawData);
    printf("PointerToRelocations: %"PRIu32"\n", section->PointerToRelocations);
    printf("PointerToLineNumbers: %"PRIu32"\n", section->PointerToLineNumbers);
    printf("NumberOfRelocations: %"PRIu16"\n", section->NumberOfRelocations);
    printf("NumberOfLineNumbers: %"PRIu16"\n", section->NumberOfLineNumbers);
    printf("Characteristics: %"PRIu32"\n", section->Characteristics);
}
