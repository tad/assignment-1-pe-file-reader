//
// Created by Never on 3/2/2023.
//

/// @file
#ifndef SECTION_EXTRACTOR_CONSTANT_H
#define SECTION_EXTRACTOR_CONSTANT_H
/// @var number of expected arguments
#define NUM_OF_ARGS 4

/// @var error message notify when receive unexpected number of arguments
#define NUM_OF_ARGS_ERROR "Number of arguments is not expected"

/// @var error message when cannot open file
#define OPEN_FILE_ERR "Cannot open file"

/// @var error message when cannot read file
#define READ_FILE_ERROR "Cannot read file"

/// @var error message when cannot write data from file to object
#define WRITE_FILE_ERROR "Cannot write data to object"

/// @var error message when cannot extract file
#define EXTRACT_FILE_ERROR "Cannot extract file!"

/// @var error message when cannot close file correctly
#define CLOSE_FILE_ERROR "Cannot close file"

/// @var return code when program run correctly
#define SUCCESS 0

/// @var return code when program cannot run correctly
#define UNEXPECTED_ERROR (-1)
#endif //SECTION_EXTRACTOR_CONSTANT_H
