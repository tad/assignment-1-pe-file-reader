//
// Created by Never on 2/28/2023.
//
/// @file
#ifndef LAB1_UTILS_H
#define LAB1_UTILS_H

#include "PE-file.h"
#include "PE-header.h"
#include <inttypes.h>

/// @brief Print description of file header
/// @param to_print pointer to file header needed to print
void print_header(struct PEHeader* to_print);

/// @brief Print description of dos header
/// @param to_print pointer to dos header needed to print
void print_dos_header(struct DOSHeader* to_print);

/// @brief Print description of section header
/// @param to_print pointer to section header needed to print
void print_section_header(struct SectionHeader *section);
#endif //LAB1_UTILS_H
